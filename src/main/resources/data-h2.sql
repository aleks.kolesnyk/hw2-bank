INSERT INTO public.customers (name, email, age) VALUES
    ('Kate Smith', 'KateSmith@gmail.com', 31),
    ('Lee Jordan', 'LeeJordan@gmail.com', 22),
    ('Brain Voo', 'BrainVoo@gmail.com', 40),
    ('Antony Sheet', 'AntonySheet@gmail.com', 35);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('82560eb9-c8f9-4be9-aaef-17fd129cec6e', 'UAH', 0, 1),
    ('f02683e2-519e-487b-b855-5286182ad1d4', 'USD', 0, 1),
    ('6295e6db-667e-4d1b-adc1-dce573b01942', 'UAH', 0,2),
    ('477b0882-84a5-41c3-87d8-a3b26cb7278f', 'EUR', 0,2),
    ('45d91b68-a4dd-4de3-88dd-13e106b12a0c', 'EUR', 0,3),
    ('4e204fcd-d296-483c-964c-74d0be4130bb', 'CHF', 0,3),
    ('9db430b11-6b5c-4d63-9663-4f59974ea04', 'USD', 0,3),
    ('6e49c4c9-c93a-48af-a37c-0af6d1db9d7d', 'UAH', 0,3),
    ('4f3d9294-a79e-48b5-8109-c934727f8edf', 'UAH', 0,4);

INSERT INTO employers
    (name, address)
    VALUES
    ('TTS', 'Stepnaya, 2'),
    ('TK', 'Polevaya, 13'),
    ('GreatCompany', 'Lesnaya, 44');

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (1, 1),
    (1, 2),
    (2, 1),
    (2, 2),
    (3, 3),
    (4, 3),
    (4, 1);
