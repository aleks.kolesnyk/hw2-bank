BEGIN;

INSERT INTO customers (name, email, age) VALUES ('Kate Smith', 'KateSmith@gmail.com', 31);
INSERT INTO customers (name, email, age) VALUES ('Lee Jordan', 'LeeJordan@gmail.com', 22);
INSERT INTO customers (name, email, age) VALUES ('Brain Voo', 'BrainVoo@gmail.com', 40);
INSERT INTO customers (name, email, age) VALUES ('Antony Sheet', 'AntonySheet@gmail.com', 35);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('82560eb9-c8f9-4be9-aaef-17fd129cec6e', 'UAH', 0, 1);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('f02683e2-519e-487b-b855-5286182ad1d4', 'USD', 0, 1);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('6295e6db-667e-4d1b-adc1-dce573b01942', 'UAH', 0,2);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('477b0882-84a5-41c3-87d8-a3b26cb7278f', 'EUR', 0,2);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('45d91b68-a4dd-4de3-88dd-13e106b12a0c', 'EUR', 0,3);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('4e204fcd-d296-483c-964c-74d0be4130bb', 'CHF', 0,3);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('9db430b11-6b5c-4d63-9663-4f59974ea04', 'USD', 0,3);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('6e49c4c9-c93a-48af-a37c-0af6d1db9d7d', 'UAH', 0,3);

INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('4f3d9294-a79e-48b5-8109-c934727f8edf', 'UAH', 0,4);

INSERT INTO employers
    (name, address)
    VALUES
    ('Roga i Kopyta', 'Turgenievska, 5');

INSERT INTO employers
     (name, address)
     VALUES
     ('Firrst and Last', 'Zapagorbom, 22');

INSERT INTO employers
     (name, address)
     VALUES
     ('Greate Chance', 'Neverland, 15');

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (1, 1);

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (1, 2);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (2, 1);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (2, 2);

 INSERT INTO customerEmployment
     (customer_id, employer_id)
     VALUES
     (3, 3);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (4, 3);

  INSERT INTO customerEmployment
      (customer_id, employer_id)
      VALUES
      (4, 1);

COMMIT;
