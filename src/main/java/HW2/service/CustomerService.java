package HW2.service;


import HW2.dao.AccountDao;
import HW2.dao.CustomerDao;
import HW2.domain.Account;
import HW2.domain.Currency;
import HW2.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerDao customerDao;
    private AccountDao accountDao;

    public CustomerService() {}

    @Autowired
    public CustomerService(CustomerDao customerDao, AccountDao accountDao) {

        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    public Customer getCustomerFullInfo(Long id) {

        return customerDao.getOne(id);
    }

    public List<Customer> getAllCustomers() {

        return customerDao.findAll();
    }

    public Customer createCustomer(Customer newCustomer) {

        return customerDao.save(newCustomer);
    }

    public Customer createCustomer(String name, String email, Integer age) {

        Customer newCustomer = new Customer(name, email, age);
        return customerDao.save(newCustomer);
    }

    public Customer updateCustomer(Customer customer) {

        if (customer.getId() == null) {
            return null;
        }
        return customerDao.save(customer);
    }

    public Customer updateCustomer(Long id, String name, String email, Integer age) {

        if (id == null) {
            return null;
        }
        Customer customer = new Customer(name, email, age);
        customer.setId(id);
        return customerDao.save(customer);
    }

    public boolean deleteCustomer(Customer customer) {

        return customerDao.delete(customer);
    }

    public boolean deleteCustomerById(Long id) {

        Customer currentCustomer = getCustomerFullInfo(id);
        return deleteCustomer(currentCustomer);
    }

    public Customer createAccount(Customer customer, Currency currency) {

        Account newAccount = new Account(customer, currency);
        accountDao.save(newAccount);
        return getCustomerFullInfo(customer.getId());
    }

    public Customer deleteAccount(Customer customer, String number) {

        Account account = accountDao.findByNumber(number);
        if (account.getCustomer().equals(customer)) {
            accountDao.delete(account);
        }
        return getCustomerFullInfo(customer.getId());
    }
}

