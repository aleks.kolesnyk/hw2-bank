package HW2.service;

import HW2.dao.EmployerDao;
import HW2.domain.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployerService {

    private EmployerDao employerDao;

    public EmployerService() {}

    @Autowired
    public EmployerService(EmployerDao employerDao) {

        this.employerDao = employerDao;
    }

    public Employer getEmployerFullInfo(Long id) {

        return employerDao.getOne(id);
    }

    public List<Employer> getAllEmployers() {

        return employerDao.findAll();
    }

    public Employer createEmployer(Employer newEmployer) {

        return employerDao.save(newEmployer);
    }

    public Employer createEmployer(String name, String address) {

        Employer newEmployer = new Employer(name, address);
        return employerDao.save(newEmployer);
    }

    public Employer updateEmployer(Employer employer) {

        if (employer.getId() == null) {
            return null;
        }
        return employerDao.save(employer);
    }

    public Employer updateEmployer(Long id, String name, String address) {

        if (id == null) {
            return null;
        }
        Employer employer = new Employer(id, name, address);
        return employerDao.save(employer);
    }

    public boolean deleteEmployer(Employer employer) {

        return employerDao.delete(employer);
    }

    public boolean deleteEmployerById(Long id) {

        Employer currentEmployer = this.getEmployerFullInfo(id);
        return deleteEmployer(currentEmployer);
    }
}

