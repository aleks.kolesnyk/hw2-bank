package HW2.service;


import HW2.dao.AccountDao;
import HW2.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountDao accountDao;

    public boolean depositAccount(String number, Double amount){
        Account account = accountDao.findByNumber(number);
        if(account != null){
            return depositAccount(account, amount);
        }
        return false;
    }
    public boolean depositAccount(Account account, Double amount){
        account.setBalance(account.getBalance() + amount);
        return true;
    }
    public boolean withdrawalMoney(String number, Double amount){
        Account account = accountDao.findByNumber(number);
        if(account.getBalance() >= amount){
            return withdrawalMoney(account, amount);
        }
        return false;
    }
    public boolean withdrawalMoney(Account account, Double amount){
        if(account.getBalance() >= amount){
            account.setBalance(account.getBalance() - amount);
            return true;
        }
        return false;
    }
    public boolean sendMoney(String numberSender, String numberReceiver, Double amount){
        Account accountSender = accountDao.findByNumber(numberSender);
        Account accountReceiver = accountDao.findByNumber(numberReceiver);
        if(!accountSender.getCurrency().equals(accountReceiver.getCurrency())){
            return false;
        }
        if(!this.withdrawalMoney(accountSender, amount)){
            return false;
        };
        if(!this.depositAccount(accountReceiver, amount)){
            this.depositAccount(accountSender, amount);
            return false;
        };
        return true;
    }
}
