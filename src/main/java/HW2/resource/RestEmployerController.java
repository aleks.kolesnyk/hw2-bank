package HW2.resource;

import HW2.domain.Employer;
import HW2.service.EmployerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employers")

public class RestEmployerController {
    @Autowired
    private EmployerService employerService;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployers(){
        return ResponseEntity.ok(employerService.getAllEmployers());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        Employer employer = employerService.getEmployerFullInfo(id);
        return employer != null
                ? ResponseEntity.ok().body(employer)
                : ResponseEntity.badRequest().body("Employer with id " + id + " not found");
    }
    @PostMapping("/")
    public ResponseEntity<?> postNewUser(@RequestBody Employer employer){
        Employer newEmployer = employerService.createEmployer(employer);
        return newEmployer != null
                ? ResponseEntity.ok().body(newEmployer)
                : ResponseEntity.badRequest().body("Saving of the new employer has fail");
    }
    @PutMapping("/")
    public ResponseEntity<?> updateUser(@RequestBody Employer employer){
        Employer newEmployer = employerService.updateEmployer(employer);
        return newEmployer != null
                ? ResponseEntity.ok().body(newEmployer)
                : ResponseEntity.badRequest().body("Updating of the employer has fail");
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return employerService.deleteEmployerById(id)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Employer not found");
    }

}
