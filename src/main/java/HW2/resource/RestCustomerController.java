package HW2.resource;

import HW2.domain.Currency;
import HW2.domain.Customer;
import HW2.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class RestCustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers(){
        return ResponseEntity.ok(customerService.getAllCustomers());
    }
    @GetMapping("/{id}")
    public Customer getUserFullInfo(@PathVariable Long id){
        return customerService.getCustomerFullInfo(id);
    }
    @PostMapping("/")
    public Customer postNewUser(@RequestBody Customer customer){
        return customerService.createCustomer(customer);
    }
    @PutMapping("/")
    public Customer updateUser(@RequestBody Customer customer){
        return customerService.updateCustomer(customer);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String currency) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode newAccountCurrency = objectMapper.readTree(currency);
        String curren = newAccountCurrency.get("currency").asText();
        Currency curr = Currency.forValue(curren);

        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerService.createAccount(customer, curr));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody String accountNumber ) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerService.deleteAccount(customer, nameNode.get("accountNumber").asText()));
    }
}

