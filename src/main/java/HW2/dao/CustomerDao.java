package HW2.dao;

import HW2.domain.Customer;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;


@Repository
@Slf4j
public class CustomerDao implements Dao<Customer> {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Customer save(Customer customerFromArg) {
        Customer existCustomerById = null;
        Customer result = null;

        if(customerFromArg.getId() != null){
            existCustomerById = getOne(customerFromArg.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if(existCustomerById == null){
                entityManager.persist(customerFromArg);
            } else {
                entityManager.merge(customerFromArg);
            }
            entityManager.getTransaction().commit();
            result = getOne(customerFromArg.getId());
        } catch (HibernateException ex){
            log.error("Saving the customer " + customerFromArg + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean delete(Customer customerFromArg) {
        deleteById(customerFromArg.getId());
        return true;
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customers.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the list of customers " + customers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customers.forEach(el -> {
                if(el.getId() != null && this.getOne(el.getId())!= null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the list of customers " + customers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try{
            customers = entityManager.createQuery("FROM Customer", Customer.class)
                    .getResultList();
        } catch (HibernateException ex) {
            log.error("Customer not found");
        } finally {
            entityManager.close();
        }
        return customers;
    }

    @Override
    public boolean deleteById(long id) {
        Customer customer = this.getOne(id);
        if(customer == null){
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customer = (Customer) entityManager.createQuery("Select c from Customer c where c.id = :id")
                    .setParameter("id", id).getSingleResult();
            entityManager.remove(customer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the customer " + customer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public Customer getOne(long id) {
        Customer customer = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            Query query = entityManager.createQuery("Select c from Customer c where c.id = :id")
                    .setParameter("id", id);
            customer = (Customer) query.getSingleResult();
        } catch (HibernateException ex){
            log.error("Customer with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return customer;
    }

}
