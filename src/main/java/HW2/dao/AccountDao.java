package HW2.dao;


import HW2.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;

@Repository
@Slf4j
public class AccountDao implements Dao<Account> {

    @Autowired
    private CustomerDao customerDao;
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Account save(Account accountFromArg) {

        Account existAccountById = null;
        Account result = null;

        if (accountFromArg.getId() != null) {
            existAccountById = getOne(accountFromArg.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (existAccountById == null) {
                entityManager.persist(accountFromArg);
                entityManager.getTransaction().commit();
                result = findAll().stream()
                        .filter(el -> el.getNumber() == accountFromArg.getNumber()
                                && el.getCurrency() == accountFromArg.getCurrency())
                        .toList().get(0);
            } else {
                entityManager.merge(accountFromArg);
                result = getOne(accountFromArg.getId());
            }
        } catch (HibernateException ex) {
            log.error("Saving the account " + accountFromArg + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean delete(Account accountFromArg) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            accountFromArg = entityManager.find(Account.class, accountFromArg.getId());
            entityManager.getTransaction().begin();
            entityManager.remove(accountFromArg);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Deleting the account " + accountFromArg + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public void deleteAll(List<Account> accounts) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            accounts.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Deleting the list of accounts " + accounts + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Account> accounts) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            accounts.forEach(el -> {
                if (el.getId() != null && this.getOne(el.getId()) != null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Saving the list of accounts " + accounts + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Account> findAll() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("from Account e ");
        List<Account> resultList = query.getResultList();
        return resultList;
    }

    @Override
    public boolean deleteById(long id) {

        Account account = getOne(id);
        if (account == null) {
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(account);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Deleting the account " + account + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public Account getOne(long id) {

        Account account = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            account = entityManager.find(Account.class, id);
        } catch (HibernateException ex) {
            log.error("Account with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return account;
    }

    public Account findByNumber(String number) {

        Account account = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager
                .createQuery("select e from Account e where e.number = :number")
                .setParameter("number", number);
        try {
            account = (Account) query.getSingleResult();
        } catch (HibernateException ex) {
            log.error("Account with number = " + number + " not found");
        } finally {
            entityManager.close();
        }
        return account;
    }
}
