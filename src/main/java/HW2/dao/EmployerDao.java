package HW2.dao;

import HW2.domain.Employer;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;

@Repository
@Slf4j
public class EmployerDao implements Dao<Employer>{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    @Override
    public Employer save(Employer employerFromArg) {
        Employer existEmployerById = null;
        Employer result = null;

        if(employerFromArg.getId() != null){
            existEmployerById = getOne(employerFromArg.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if(existEmployerById == null){
                entityManager.persist(employerFromArg);
                result = findAll().stream()
                        .filter(el -> el.getName() == employerFromArg.getName()
                                && el.getAddress() == employerFromArg.getAddress())
                        .toList().get(0);
            } else {
                entityManager.merge(employerFromArg);
                result = getOne(employerFromArg.getId());
            }
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the customer " + employerFromArg + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean delete(Employer employer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(employer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the employer " + employer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public void deleteAll(List<Employer> employers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            employers.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the list of employers " + employers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Employer> employers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            employers.forEach(el -> {
                if(el.getId() != null && getOne(el.getId())!= null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the list of employers " + employers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Employer> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("from Employer e ");
        List<Employer> resultList = query.getResultList();
        return resultList;
    }

    @Override
    public boolean deleteById(long id) {
        Employer employer = getOne(id);
        if(employer == null){
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(employer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the employer " + employer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public Employer getOne(long id) {
        Employer employer = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            employer = entityManager.find(Employer.class, id);
        } catch (HibernateException ex){
            log.error("Employer with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return employer;
    }
}

