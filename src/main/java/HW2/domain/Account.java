package HW2.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;


@Entity
@Table(name = "accounts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})

public class Account extends AbstractEntity{

    @Column(name = "number")
    private String number;

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column(name = "balance")
    private Double balance;

    @JsonIgnore
    @ManyToOne
    private Customer customer;
    public Account(Customer customer, Currency currency){
        this.currency = currency;
        this.customer = customer;
        this.number = UUID.randomUUID().toString();
        this.balance = 0D;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + this.getId() +
                ", number='" + number + '\'' +
                ", currency=" + currency +
                ", balance=" + balance +
                '}';
    }

}
