package HW2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employers")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Employer extends AbstractEntity {

    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;

    @JsonIgnore
    @ManyToMany(mappedBy = "employers")
    private Set<Customer> customers = new HashSet<>();

    public Employer(String name, String address) {

        this.name = name;
        this.address = address;
    }

    public Employer(Long id, String name, String address) {

        this.setId(id);
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {

        return "Employer{" +
                "id=" + this.getId() +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

